import React, { useEffect, useState } from 'react';
import { connect, useStore } from 'react-redux';
import Loader from '../../components/Loader/Loader';
import { View } from 'react-native';
import { isNullOrUndefined, isObject } from 'util';
import { LOGIN, LOGIN_PAGE } from '../../routes/RouteConst';
import api from '../../api';
import { useNavigation } from '@react-navigation/native';
import { Toast, Root } from 'native-base';
import { getUserProfileInfo } from '../../constants/AsyncStorageHelper';

const withUser = (WrapComponent) => {
    const newComponent = (props) => {
        //const store = useStore()
        const navigation = useNavigation()
        const [userInfo, setUserInfo] = useState(undefined);
        const [loading, setLoading] = useState(true);
        const [error, setError] = useState(undefined);
        const logout = async()=> {
            // Toast.show({
            //     text: error || "Somthing went wrong.Please Login Again",
            //     buttonText: "Okay",
            //     duration: 5000,
            //     //type: "warning"
            // })
            
            await api.user.logout();
            navigation.reset({
                index: 0,
                routes: [{name: LOGIN}],
            });   
        }

        const getUserData = async () => {
            try{
                if(isNullOrUndefined(userInfo) && !isObject(userInfo)){
                    //store.dispatch(getUserInfo())
                    const userObject = await getUserProfileInfo();
                    setUserInfo(userObject)
                    setLoading(false);
                }
            }catch(err){
                logout();
                setLoading(false);
            }
        }
        useEffect(() => {
            getUserData()
        },[])
        if(error){
            logout();
            return <Root style={{flex:1}}>

            </Root>
        }
        return (
        <Root style={{flex:1}}>
            {loading ? <Loader loading={loading}></Loader> : <WrapComponent userInfo={userInfo} {...props} />}  
          </Root>
          )
    }
    return connect(mapStateToProps)(newComponent);
}

const mapStateToProps = (state) => {
    return {
      
    }
};

export default withUser;