import Base from './BaseApi';

export default class TrainerApi extends Base {
  getTrainerApi(intl, id) {
    return this.apiClient.get(intl, `api/Account/TrainerDetails/${id}`);
  }
}
