import 'react-native-gesture-handler';
import * as React from 'react';
import { useNavigation } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import RouteConst, { APP_DRAWER, LOGIN_PAGE,  } from './RouteConst'
import AppDrawer from './AppDrawer';
import { LoginPage,  } from '../Screens';
import { useSelector, useDispatch } from 'react-redux';



const MainStack = createStackNavigator();
const MainRoute = () => {
  const login_status = useSelector((state) => state.User.login_status)

  return (
    <MainStack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      {login_status ?
        (<>
          <MainStack.Screen name={APP_DRAWER} component={AppDrawer} />
      
        </>
        )
        :
        (<MainStack.Screen name={LOGIN_PAGE} component={LoginPage} />)
      }
    </MainStack.Navigator>
  );
}
export default MainRoute;