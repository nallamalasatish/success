import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  View,
  Text,
  Button,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Image,
  Pressable,
} from 'react-native';
import {Common} from '../../assets';
import {Loader} from '../../components/Loader/Loader';
import {
  BLACK_COLOR,
  FONT_BOLD,
  SMALL_FONT_SIZE,
  STANDARD_SCREEN_HEIGHT,
  WHITE_COLOR,
} from '../../constants/AppConst';
import {RFValue} from 'react-native-responsive-fontsize';
import Metrics from '../../constants/metrics';
import {Formik} from 'formik';
import {loginInitialValues, LoginValidaionSchema} from './LoginHelper';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { COLORS } from '../../constants';

const LoginComponent = props => {
  const {onLogin} = props;
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const [viewPassword, setViewPassword] = useState(true);
  return (
    <ScrollView>
      <View
        style={{
          flex: 1,
          height: heightPercentageToDP('100%'),
          alignIself: 'center',
        }}>
        <ImageBackground
          source={Common.loginBack}
          style={{
            alignItems: 'center',
          }}
          imageStyle={{
            height: heightPercentageToDP('100%'),
          }}>
          <Loader loading={loading}></Loader>
          <Formik
            initialValues={loginInitialValues(props)}
            validationSchema={LoginValidaionSchema(props)}
            onSubmit={values => onLogin(values)}>
            {({
              values,
              handleChange,
              setFieldValue,
              errors,
              touched,
              setFieldTouched,
              isValid,
              handleSubmit,
            }) => (
              <View
                style={{
                  marginTop: RFValue(200, STANDARD_SCREEN_HEIGHT),
                  width: Metrics.rfv(380),
                  maxWidth: '70%',
                  backgroundColor: '#fff',
                  borderRadius: Metrics.rfv(30),
                  elevation: Metrics.rfv(8),
                  borderWidth: Metrics.rfv(0),
                  display: 'flex',
                  alignItems: 'center',
                  flexDirection: 'column',
                  padding: Metrics.rfv(10),
                }}>
                <Image
                  source={Common.samvaadlogo}
                  style={{marginTop: Metrics.rfv(20)}}
                />
                <Text
                  style={{
                    fontSize: Metrics.rfv(22),
                    color: '#8c8c8c',
                    fontWeight: '700',
                    marginTop: Metrics.rfv(10),
                  }}>
                  Welcome to Samvaad
                </Text>
                <Text
                  style={{
                    fontSize: Metrics.rfv(15),
                    color: '#8c8c8c',
                    fontWeight: '400',
                  }}>
                  Learning Management System
                </Text>
                <TextInput
                  placeholder="Email"
                  placeholderTextColor="#8c8c8c"
                  value={values.userName}
                  onChangeText={t => setFieldValue('userName', t)}
                  style={{
                    width: '90%',
                    borderBottomColor: '#555555',
                    borderBottomWidth: Metrics.rfv(2),
                    paddingBottom: Metrics.rfv(5),
                    fontSize: Metrics.rfv(17),
                    marginTop: Metrics.rfv(20),
                    color: '#8c8c8c',
                  }}
                />
                {errors.userName && touched.userName && (
                  <Text style={{fontSize: 15, color: 'red',}}>
                    {errors.userName}
                  </Text>
                )}
                <View
                  style={{
                    width: '90%',
                    marginTop: Metrics.rfv(10),
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    borderBottomColor: '#555555',
                    borderBottomWidth: Metrics.rfv(2),
                  }}>
                  <TextInput
                    placeholder="Password"
                    placeholderTextColor="#8c8c8c"
                    value={values.password}
                    onChangeText={t => setFieldValue('password', t)}
                    error={errors.password}
                    touched={touched.password}
                    secureTextEntry={viewPassword}
                    style={{
                      flex: 1,
                      paddingBottom: 5,
                      fontSize: 17,
                      color: '#8c8c8c',
                    }}
                  />
                  <Icon
                    name={!viewPassword ? 'eye' : 'eye-slash'}
                    color={COLORS.blue}
                    size={Metrics.rfv(20)}
                    style={{
                      textAlignVertical: 'center',
                    }}
                    onPress={() => {
                      setViewPassword(!viewPassword);
                    }}
                  />                
                </View>
                {errors.password && touched.password && (
                    <Text style={{fontSize: 15, color: 'red',}}>
                      {errors.password}
                    </Text>
                  )}
                <TouchableOpacity
                  onPress={handleSubmit}
                  style={{
                    width: '90%',
                    marginTop: Metrics.rfv(40),
                    marginBottom: Metrics.rfv(20),
                    borderRadius: Metrics.rfv(40),
                    backgroundColor: '#06bde6',
                    paddingVertical: 10,
                  }}>
                  <Text
                    style={{
                      color: '#fff',
                      fontWeight: '500',
                      fontSize: Metrics.rfv(20),
                      textAlign: 'center',
                    }}>
                    LOGIN
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </Formik>
        </ImageBackground>
      </View>
    </ScrollView>
  );
};
export default LoginComponent;
