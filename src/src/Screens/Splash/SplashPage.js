import  React, {useEffect} from 'react';
 import {View, Text, ImageBackground} from 'react-native';
import { withGlobalize } from 'react-native-globalize';
import {useNavigation} from '@react-navigation/native';
import IntlProvider from '../../../src/constants/IntlProvider';
import { getUserProfileInfo } from '../../../src/constants/AsyncStorageHelper';
import { isObject, isNullOrUndefined } from 'util';
import { MAIN_ROUTE, LOGIN_PAGE, LOGIN, STUDENT_ROUTE } from '../../routes/RouteConst';
import { Common } from '../../assets';

export const splashIntlProvider = (props) => ({
    appName: IntlProvider(props, 'app/appName'),
})

const SplashPage = (props) => {
    // const login_status = useSelector((state) => state.User.login_status)
    const navigation = useNavigation()
    
    const navigationStep = async() => {
        const userObject = await getUserProfileInfo();
        setTimeout(() => {
            if(isObject(userObject) && !isNullOrUndefined(userObject.USERID)){
                navigation.navigate(MAIN_ROUTE)
            }else {
                navigation.navigate(LOGIN_PAGE)
            }
        }, 3000);
    }

    useEffect(() => {
        navigationStep();
    }, [])


    return <ImageBackground 
            style={{flex:1, justifyContent: 'center', alignItems: 'center', resizeMode:'cover'}}
            source={Common.SplashImg}
            resizeMode={"cover"}
            >
                
        </ImageBackground>
}

export default withGlobalize(SplashPage);