import { useNavigation } from '@react-navigation/native'
import React from 'react'
import { View, Text, Button } from 'react-native'
import AppHeader from '../../components/AppHeader/AppHeader';

const TrainerHome = () => {
    const navigation = useNavigation();

    return (
        <View>
             <AppHeader  menuIcon={true} title={'Trainer Home'} rightIcon={true}/>
            <Text> Trainer Home</Text>
        </View>
    )
}
export default TrainerHome;