import _TrainerHome from './Home/TrainerHome';
import _LoginPage from './Login/LoginContainer';
import _StudentHome from './Home/StudentHome';
import _SplashPage from './Splash/SplashPage';

export const TrainerHome = _TrainerHome;
export const LoginPage = _LoginPage;
export const StudentHome = _StudentHome;
export const SplashPage = _SplashPage;