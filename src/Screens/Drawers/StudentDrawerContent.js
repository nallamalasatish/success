import { StyleSheet, Text, View, Dimensions, Image, Pressable,Alert } from 'react-native'
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import Poll from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import Fontisto from 'react-native-vector-icons/Fontisto'
import { useSelector, useDispatch } from 'react-redux'
import Metrics from '../../constants/metrics';
import {
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
} from '@react-navigation/drawer';
import { useNavigation,DrawerActions } from '@react-navigation/native';
import { logout } from '../../Redux/reducer/User';
import api from '../../api'
import { COLORS } from '../../constants';


const StudentDrawerContent = ({ ...props }) => {
    const navigation = useNavigation();
    const dispatch = useDispatch()

    const onLogoutPress = async () => {
        await api.user.logout();
        dispatch(logout())
      };

    return (
        <View style={{ flex: 1, backgroundColor: COLORS.blue, }}>
            <DrawerContentScrollView {...props}>

                <LinearGradient
                    colors={['#3E48A0', '#3E48A0']}

                    style={{
                        marginTop: Metrics.rfv(-4),
                        flexDirection: 'row',
                    }}>

                    <Entypo name="cross" color={"#FFFFFF"} size={Metrics.rfv(20)}
                        style={{ marginTop: 20 }}
                        onPress={() => navigation.dispatch(DrawerActions.closeDrawer())}
                    />

                    <Poll name="logout" color={"#FFFFFF"} size={Metrics.rfv(20)}
                        style={{ marginLeft: Metrics.rfv(150), marginTop: Metrics.rfv(30) }}
                        onPress={() => {
                            Alert.alert("Logout", "Are you want Logout ?",
                                [
                                    { text: "Cancel", onPress: () => { } },
                                    { text: "Ok", onPress: () => { onLogoutPress() } }
                                ])
                        }}
                    />

                </LinearGradient>
                <DrawerItem
                    labelStyle={{ color: '#FFFFFF', fontSize: Metrics.rfv(18) }}
                    icon={() => (
                        <Ionicons
                            name="home"
                            style={{ color: '#EDEDED', fontSize: Metrics.rfv(18) }}
                        />
                    )}
                    label="Student Home"
                    onPress={() => navigation.navigate('StudentHome')}
                />
                {/* <DrawerItem
                    labelStyle={{ color: '#FFFFFF', fontSize: Metrics.rfv(18) }}
                    icon={() => (
                        <Poll
                            name="poll"
                            style={{ color: "#FFFFFF", fontSize: Metrics.rfv(18) }}
                        />
                    )}
                    label="Polls & Surveys"
                    onPress={() => navigation.navigate('Poll')}
                /> */}
                {/* <DrawerItem
                    labelStyle={{ color: '#FFFFFF', fontSize: Metrics.rfv(18) }}
                    icon={() => (
                        <MaterialIcons
                            name="payment"
                            style={{ color: "#FFFFFF", fontSize: Metrics.rfv(18) }}
                        />
                    )}
                    label="Fee Payment"
                    onPress={() => navigation.navigate('Payment')}
                /> */}
                {/* <DrawerItem
                    labelStyle={{ color: '#FFFFFF', fontSize: mdscale(12) }}
                    icon={() => (
                        <MaterialIcons
                            name="wallet-giftcard"
                            style={{ color: "#FFFFFF", fontSize: mdscale(18) }}
                        />
                    )}
                    label="Tips and Wishes"
                    onPress={() => navigation.navigate('TipsWishes')}
                /> */}
                {/* <DrawerItem
                    labelStyle={{ color: '#FFFFFF', fontSize: mdscale(12) }}
                    icon={() => (
                        <MaterialIcons
                            name="time-to-leave"
                            style={{ color: "#FFFFFF", fontSize: mdscale(18) }}
                        />
                    )}
                    label="Transport"
                    onPress={() => navigation.navigate('Transport')}
                /> */}

            </DrawerContentScrollView>
        </View>
    )
}

export default StudentDrawerContent

const styles = StyleSheet.create({})