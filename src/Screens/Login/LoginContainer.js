import React, { useState, useEffect } from 'react'
import LoginComponent from './LoginComponent'
import { signInIntlProvider } from './LoginHelper'
import api from '../../api'
import { withGlobalize } from 'react-native-globalize'
import { useNavigation, useIsFocused } from '@react-navigation/native'
import { MAIN_ROUTE } from '../../routes/RouteConst'
import { saveUserProfileInfo } from '../../constants/AsyncStorageHelper'
import { useDispatch, useSelector } from 'react-redux'
import { setuser } from '../../Redux/reducer/User'

const LoginPage = (props) => {
    const intl = signInIntlProvider(props);
    //const [loading, setLoading] = useState(false)
    const navigation = useNavigation();
    const isFocused = useIsFocused();
    const login_status = useSelector((state) => state.User.login_status)
    const dispatch = useDispatch();

    useEffect(() => { }, [isFocused])
    const onLogin = async (values) => {
        //setLoading(true)
        const payload = {
            password: values.password,
            userName: values.userName
        }
        const res = await api.user.login(intl, payload)
        console.log('res', res.data)
        // const userInfo = res.data
        const userInfo = {Id:101}
        await saveUserProfileInfo(userInfo);
        dispatch(setuser(userInfo))
        navigation.reset({
            index: 0,
            routes: [{ name: MAIN_ROUTE }],
        })
    }

    return (
        <LoginComponent
            onLogin={onLogin}
            {...props}
        />
    )
}
export default withGlobalize(LoginPage);