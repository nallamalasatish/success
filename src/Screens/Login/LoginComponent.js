import React, { memo, useState } from 'react';
import {
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  View, Text,
  TouchableWithoutFeedback, Modal, Image, ScrollView, Dimensions
} from 'react-native';

import { Common } from '../../assets';
import { APPLICATION_STYLES } from '../../constants';
import Metrics from '../../constants/metrics';
import { loginInitialValues, LoginValidaionSchema } from './LoginHelper';
import { AppText } from '../../components';
import { Formik } from 'formik';
import AppTextField from '../../components/AppTextInput/AppTextInput';
import AppTextFieldPassword from '../../components/AppTextFieldPassword/AppTextFieldPassword';
import AppButton from '../../components/AppButton/AppButton';
import { COLORS } from '../../constants';
import { SIGN_PAGE } from '../../routes/RouteConst';
import { AppOkAlert } from '../../constants/AlertHelper';
import { withGlobalize } from 'react-native-globalize';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { RFValue } from 'react-native-responsive-fontsize';
import { STANDARD_SCREEN_HEIGHT } from '../../constants/AppConst';
import { BackHandler } from 'react-native';
import { useEffect } from 'react';
import { Alert } from 'react-native';

const { width, height } = Dimensions.get('window')

const LoginComponent = withGlobalize(memo(props => {
  const { intl, onLogin,  } = props;
  const navigation = useNavigation();
  // const [popUpView, setPopUpView] = useState(false);
  return (
    <ImageBackground
      imageStyle={{
        height: height * 1,
        width: width * 1,
        // borderRadius: Metrics.rfv(0),
        //position: 'absolute'
      }}
      source={Common.SplashImg}
      resizeMode={'stretch'}
    >
      <ScrollView automaticallyAdjustKeyboardInsets={true}>
        <View
          // style={[
          //   { ...styles.CARD },
          //   { ...styles.SHADOW_EFFECT },
          //   { ...styles.DEFAULT_CARD_STYLE },
          // ]}
          style={{
            width: Metrics.rfv(330),
            height: Metrics.rfv(420),
            alignSelf: 'center',
            marginTop: Metrics.rfv(200),
            backgroundColor: '#FFFFFF',
            borderRadius: 10,
          }}
        >
          <View style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignSelf: 'center',
            marginRight: Metrics.rfv(15),
            marginTop: Metrics.rfv(10)
          }}>
  
            <Image
              style={{
                height: Metrics.rfv(55),
                width: Metrics.rfv(230),
              }}
              source={Common.samvaadlogo}
              resizeMode={"cover"}
            >
            </Image>
          </View>
          <Formik
            initialValues={loginInitialValues(props)}
            validationSchema={LoginValidaionSchema(props)}
            onSubmit={(values, { resetForm }) => {
              onLogin(values,resetForm);
            }}>
            {({
              values,
              handleChange,
              setFieldValue,
              errors,
              touched,
              setFieldTouched,
              isValid,
              handleSubmit,
            }) => (
              <>
                <AppText style={{ color: 'black', marginLeft: Metrics.rfv(22), marginTop: Metrics.rfv(10) }}>
                  Enter user Name
                </AppText>
                <AppTextField
                  placeHolder={'Enter Email Address'}
                  value={values.userName}
                  //value={"sheshubompally@gmail.com"}
                  changeText={handleChange('userName')}
                  onBlur={() => setFieldTouched('userName')}
                  autoCapitalize={'none'}
                  // maxLength={10}
                  touched={touched.userName}
                  error={errors.userName}
                />
                <AppText style={{ color: 'black', marginLeft: Metrics.rfv(22), marginTop: Metrics.rfv(10) }}>
                  Enter Password
                </AppText>
                <AppTextFieldPassword
                  placeHolder={'Password'}
                  value={values.password}
                  changeText={handleChange('password')}
                  onBlur={() => setFieldTouched('password')}
                  secureTextEntry={true}
                  maxLength={8}
                  touched={touched.password}
                  error={errors.password}
                />
                <AppButton
                  colors={[COLORS.maroon, COLORS.maroon]}
                  containerStyle={{
                    marginTop: Metrics.rfv(10),
                    width: '90%',
                    paddingLeft: Metrics.rfv(20),
                    paddingRight: Metrics.rfv(20),
                    alignSelf: 'center',
                  }}
                  textStyle={{
                    color: COLORS.white,
                  }}
                  onPress={() => {
                    // setPopUpView(true);
                    handleSubmit();
                  }}
                  label={'Login'}
                />
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                    justifyContent: 'center',
                    marginTop: Metrics.rfv(10),
                    width: '90%',
                    height: Metrics.rfv(30),
                  }}>
                  <AppText style={{ color: '#000000' }}>Don't have a account ? </AppText>
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('RegisterPage');
                    }}
                  >
                    <AppText style={{ color: '#98280B' }}>Sign Up</AppText>
                  </TouchableOpacity>
                </View>
                <TouchableOpacity
                  style={{
                    alignSelf: 'center',
                    justifyContent: 'center',
                    marginTop: Metrics.rfv(0),
                  }}
                  onPress={() => {
                    if (values.mobile == '') {
                      AppOkAlert('Please enter user name', () => { });
                      return;
                    }
                   
                  }}>
                  <AppText style={{ color: '#98280B' }}>Forgot password?</AppText>
                </TouchableOpacity>
              </>
            )}
          </Formik>
        </View>
      </ScrollView>
    </ImageBackground >
  );
}));

const styles = StyleSheet.create({
  ...APPLICATION_STYLES,
});
export default LoginComponent;
