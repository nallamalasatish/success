import * as yup from 'yup';
import IntlProvider from '../../constants/IntlProvider';


export const loginInitialValues = (props) => ({
    userName: '', //'varsha@finance.com',
    password: '', //'123456',
});

export const LoginValidaionSchema = (props) => {
    return yup.object().shape({
        userName: yup
            .string()
            .email('Please Enter a Valid Email')
            .required('Email is Required'),
        password: yup
            .string()
            .required('Password Required'),
    });
};

export const signInIntlProvider = (props) => ({
    appName: IntlProvider(props, 'app/appName'),
    noInternet: IntlProvider(props, 'app/noInternet'),
    somethingWentWrong: IntlProvider(props, 'app/somethingWentWrong'),
    userNamePlaceholder: IntlProvider(props, 'inputPlaceHolder/userName'),
    passwordPlaceholder: IntlProvider(props, 'inputPlaceHolder/password'),
    mobileNumberPlaceholder: IntlProvider(props, 'inputPlaceHolder/mobileNumber'),
    usernameEmailPlaceholder: IntlProvider(
        props,
        'inputPlaceHolder/usernameEmail',
    ),
    generateOTPPlaceholder: IntlProvider(props, 'inputPlaceHolder/generateOTP'),
    OTPPlaceholder: IntlProvider(props, 'inputPlaceHolder/OTP'),
    usernameNotValid: IntlProvider(props, 'inputValidation/usernameNotValid'),
    userNameValidation2: IntlProvider(
        props,
        'inputValidation/userNameValidation2',
    ),
    userNameValidation3: IntlProvider(
        props,
        'inputValidation/userNameValidation3',
    ),
    userNameRegExpValidation: IntlProvider(
        props,
        'inputValidation/userNameRegExpValidation',
    ),
    userNameMin: IntlProvider(props, 'inputValidation/userNameMin'),
    userNameMax: IntlProvider(props, 'inputValidation/userNameMax'),
    userNameReq: IntlProvider(props, 'inputValidation/userNameReq'),
    passwordRegExpValidation: IntlProvider(
        props,
        'inputValidation/passwordRegExpValidation',
    ),
    passwordMin: IntlProvider(props, 'inputValidation/passwordMin'),
    passwordMax: IntlProvider(props, 'inputValidation/passwordMax'),
    passwordReq: IntlProvider(props, 'inputValidation/passwordReq'),
    login: IntlProvider(props, 'loginScreen/login'),
    forgotPassword: IntlProvider(props, 'loginScreen/forgotPassword'),
    registerNow: IntlProvider(props, 'loginScreen/registerNow'),
    parentLogin: IntlProvider(props, 'loginScreen/parentLogin'),
    studentLogin: IntlProvider(props, 'loginScreen/studentLogin'),
    somethingWentWrong: IntlProvider(props, 'errorMsg/somethingWentWrong'),
    userNamePasswordWrong: IntlProvider(props, 'errorMsg/userNamePasswordWrong'),
    noUserFound: IntlProvider(props, 'errorMsg/noUserFound'),
    somethingWrongTryAgain: IntlProvider(
        props,
        'errorMsg/somethingWrongTryAgain',
    ),
    mobileMatchValidation: IntlProvider(props, 'inputValidation/mobileMatch'),
    mobileRequiredValidation: IntlProvider(
        props,
        'inputValidation/mobileRequired',
    ),
    onlyDigitValidation: IntlProvider(props, 'inputValidation/onlyDigit'),
    otpLengthValidation: IntlProvider(props, 'inputValidation/otpLength'),
    otpRequiredValidation: IntlProvider(props, 'inputValidation/otpRequired'),
    lmsLogin: IntlProvider(props, 'loginScreen/lmsLogin'),
    student: IntlProvider(props, 'homePage/student'),
    parent: IntlProvider(props, 'homePage/parent'),
    otpSendMessage: IntlProvider(props, 'inputValidation/otpSendMessage'),
});