import { useNavigation } from '@react-navigation/native';
import React from 'react'
import { Text, View, Button } from 'react-native';

import { useDispatch } from 'react-redux';

import AppHeader from '../../components/AppHeader/AppHeader';

const StudentHome = () => {

    const dispatch = useDispatch()
   

    const navigation = useNavigation();
    return (
        <View>
            <AppHeader  menuIcon={true} title={'Student Home'} rightIcon={true}/>
            <Text>Student Home</Text>
            
        </View>
    )
}
export default StudentHome;