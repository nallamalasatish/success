import Base from './BaseApi';
import { saveUserId, saveUserProfileInfo } from '../constants/AsyncStorageHelper';

export default class UserAPI extends Base {
  login(intl, data) {
    return this.apiClient.post(intl, 'api/Account/Login', data);
  }
  async logout() {
    await saveUserProfileInfo({});
  }
}
