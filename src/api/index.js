import ApiClient from './ApiClient';
import UserAPI from './UserAPI';
import TrainerApi from './TrainerApi';

export const apiClient = new ApiClient();

const combinedAPI = {
  user: new UserAPI(apiClient),
  trainers: new TrainerApi(apiClient),
};
export default combinedAPI;
