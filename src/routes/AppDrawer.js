import 'react-native-gesture-handler';
import * as React from 'react';
import { createDrawerNavigator } from "@react-navigation/drawer";
import * as RouteConst from './RouteConst';
import { widthPercentageToDP } from 'react-native-responsive-screen';

import StudentDrawerContent from '../Screens/Drawers/StudentDrawerContent';
import TrainerDrawerContent from '../Screens/Drawers/TrainerDrawerContent';
import { getUserProfileInfo } from '../constants/AsyncStorageHelper';
import StudentHome from '../Screens/Home/StudentHome';
import { TrainerHome } from '../Screens';
const Drawer = createDrawerNavigator();

export const AppDrawer = (props) => {

  // const [roleId, setRoleId] = React.useState();
  // const getLogin = async () => {
  //   const response = await getUserProfileInfo();
  //   console.log('res Drawer', response.ROLEID)
  //   setRoleId(response.ROLEID)
  // }
  // React.useEffect(() => {
  //   getLogin();
  // }, [])

  // if (roleId && roleId == 2) {
    return (
      <Drawer.Navigator
        drawerStyle={{
          //backgroundColor: THEME_COLOR
          width: widthPercentageToDP('70%')
        }}
        screenOptions={{
          headerShown: false,
          //headerTransparent:true
        }}
        initialRouteName={RouteConst.TRAINER_HOME}
        drawerContent={() => <TrainerDrawerContent />}>

        <Drawer.Screen name={RouteConst.TRAINER_HOME} component={TrainerHome} />
      </Drawer.Navigator>
    )
  // }
  // else {
  //   return (
  //     <Drawer.Navigator
  //       drawerStyle={{
  //         //backgroundColor: THEME_COLOR
  //         width: widthPercentageToDP('70%')
  //       }}
  //       screenOptions={{
  //         headerShown: false,
  //         //headerTransparent:true
  //       }}
  //       initialRouteName={RouteConst.STUDENT_HOME}
  //       drawerContent={() => <StudentDrawerContent />}>

  //       <Drawer.Screen name={RouteConst.STUDENT_HOME} component={StudentHome} />

  //     </Drawer.Navigator>
  //   )
  // }
}

export default AppDrawer;