export const SPLASH_PAGE = "SplashPage";
export const LOGIN_PAGE = 'LoginPage';
export const MAIN_ROUTE = "MainRoute";
export const TRAINER_HOME = 'TrainerHome';
export const APP_DRAWER = "AppDrawer";
export const STUDENT_HOME = "StudentHome";
