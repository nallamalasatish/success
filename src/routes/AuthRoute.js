import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MainRoute from './MainRoute';
import RouteConst, { LOGIN_PAGE, MAIN_ROUTE, SPLASH_PAGE } from './RouteConst'
import { LoginPage, SplashPage } from '../Screens';
const Stack = createStackNavigator();
const AuthRoute = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{
        headerShown: false
      }}
      >
        <Stack.Screen name={SPLASH_PAGE} component={SplashPage} />
        <Stack.Screen name={MAIN_ROUTE} component={MainRoute} />
        <Stack.Screen name={LOGIN_PAGE} component={LoginPage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default AuthRoute;