import React, { useEffect } from 'react';
import { View, TouchableOpacity, StyleSheet, BackHandler, Image, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useIsFocused, useNavigation, useRoute, DrawerActions } from '@react-navigation/native';
import { useDispatch, useStore } from 'react-redux';
import { withGlobalize } from 'react-native-globalize';
import { COLORS } from '../../constants/colors';
import Metrics from '../../constants/metrics';
import { AppText } from '../AppText/text';
import api from '../../api';
import { Common } from '../../assets/index';
import { memo } from 'react';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { logout } from '../../Redux/reducer/User';
import {Badge} from 'react-native-elements';
import { STANDARD_SCREEN_HEIGHT, TRANSPARENT } from '../../constants/AppConst';
import { RFValue } from 'react-native-responsive-fontsize';

export const headerBackIntlProvider = props => ({
  // wantLogout: IntlProvider(props, 'sideMenu/wantLogout')
});

const AppHeader = withGlobalize(memo(props => {
  const { title, leftIcon, rightIcon, menuIcon } = props;
  const intl = headerBackIntlProvider(props);
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const dispatch = useDispatch()


  const onBackPressed = () => {
    navigation.goBack();
    return true;
  };

  const onLogoutPress = async () => {
    await api.user.logout();
    dispatch(logout())
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', onBackPressed);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', onBackPressed);
    }
  }, [isFocused]);
  return (
    <View style={styles.mainContainer}>
      <View style={styles.container}>
        {menuIcon && (
          <TouchableOpacity
            style={[
              styles.centerView,
              styles.iconContainer,
              {
                marginLeft: Metrics.rfv(4),
                marginRight: Metrics.rfv(12),
              },
            ]}
            onPress={() => {
              navigation.dispatch(DrawerActions.openDrawer());

            }}>
            <MaterialIcons name={'menu'} color={COLORS.white} size={Metrics.rfv(30)} />
          </TouchableOpacity>
        )}
        {leftIcon && (
          <TouchableOpacity
            style={[
              styles.centerView,
              styles.iconContainer,
              {
                marginLeft: Metrics.rfv(4),
                marginRight: Metrics.rfv(12),
              },
            ]}
            onPress={() => {
              navigation.goBack();
            }}>
            <MaterialIcons name={'west'} color={COLORS.white} size={Metrics.rfv(30)} />
          </TouchableOpacity>
        )}
        <View
          style={[
            styles.textContainer,
            { marginLeft: !leftIcon ? Metrics.rfv(20) : Metrics.rfv(4) },
          ]}>
          <AppText
            preset={'HEADING_2M_20'}
            color={'white'}
            style={{ fontWeight: 'bold' }}
            tx={title}></AppText>
        </View>
        <View>
        <TouchableOpacity
          style={[
            styles.iconContainer,
            {
              backgroundColor: TRANSPARENT,
              marginRight: RFValue(10, STANDARD_SCREEN_HEIGHT),
              marginTop: RFValue(20, STANDARD_SCREEN_HEIGHT),
            },
          ]}
          onPress={() => {
            alert('notification');
          }}>
          <Icon
            name={'bell'}
            color={'white'}
            size={RFValue(30, STANDARD_SCREEN_HEIGHT)}
          />
        </TouchableOpacity>
        <Badge
          value={'0'}
          containerStyle={{
            top: Metrics.rfv(-45),
            color:'red'
          }}></Badge>
      </View>
        {rightIcon && (
          <TouchableOpacity
            style={[
              styles.centerView,
              styles.iconContainer,
              {
                marginLeft: Metrics.rfv(4),
                marginRight: Metrics.rfv(12),
              },
            ]}
            onPress={() => {
              Alert.alert("Logout", "Are you want Logout ?",
                [
                  { text: "Cancel", onPress: () => { } },
                  { text: "Ok", onPress: () => onLogoutPress() }
                ])
            }}>
            <Icon name={'power-off'} color={COLORS.white} size={Metrics.rfv(25)} />
            {/* <Image style={{ width: 30, height: 30, tintColor: 'white' }} source={Common.logout} /> */}
          </TouchableOpacity>
        )}


      </View>
    </View>
  )
}))
const styles = StyleSheet.create({
  mainContainer: {
    width: '100%',
    height: Metrics.rfv(50),
    backgroundColor: COLORS.maroon,
  },
  centerView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    width: '100%',
    height: Metrics.rfv(60),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    width: Metrics.rfv(40),
    height: Metrics.rfv(40),
  },
  leftIconContainer: {
    marginLeft: Metrics.rfv(10),
    borderRadius: Metrics.rfv(20),
    overflow: 'hidden',
    borderWidth: 0,
  },

  textContainer: {
    flex: 1,
    marginLeft: Metrics.rfv(4),
  },
});
export default withGlobalize(AppHeader);