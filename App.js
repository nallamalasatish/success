import 'react-native-gesture-handler';
import { StyleSheet, View, SafeAreaView, StatusBar, LogBox } from 'react-native'
import React, { useEffect, useState } from 'react'
import configureStore from './src/store/configureStore'
import { FormattedProvider, GlobalizeProvider } from 'react-native-globalize';
import AuthRoute from './src/routes/AuthRoute';
import { TOOLBAR_1 } from './src/constants/AppConst';
import metadata from './src/locales/index'
import { useDispatch, useSelector } from 'react-redux';
import { getUserProfileInfo } from './src/constants/AsyncStorageHelper';
import { setuser } from './src/Redux/reducer/User';
import { COLORS } from './src/constants';

console.disableYellowBox = true;
LogBox.ignoreAllLogs(true);
export const store = configureStore();

const AppStatusBar = ({ backgroundColor, ...props }) => {
  if (Platform.OS == "ios") {
    return (
      <View style={[styles.statusBar, backgroundColor]}>
        <StatusBar backgroundColor={backgroundColor} {...props} />
      </View>
    );
  } else {
    return <StatusBar backgroundColor={backgroundColor}  {...props} />
  }

};

const App = () => {
  const [locale, setLocale] = useState();

  const login_status = useSelector((state) => state.User.login_status)
  const dispatch = useDispatch()

  const checkUser = async () => {
    let account = await getUserProfileInfo()
   // console.log("Acount", account);
    if (account) {
      console.log("account", account.Id);
      dispatch(setuser(account))
    }
  }
  const setUpLocale = async () => {
    const locale = await metadata.locale();
    setLocale(locale)
  }
  useEffect(() => {
    setUpLocale()
    checkUser()
  }, [])
  return (
    <>
      <SafeAreaView style={styles.topSafeArea} />
      <SafeAreaView style={[{ flex: 1 }, { ...styles.bottomSafeArea }]}>
        <AppStatusBar backgroundColor={ COLORS.maroon} barStyle="light-content" />
        <FormattedProvider
          locale={locale}
          currency={metadata.currency()}
          messages={metadata.messages()}
          skeleton={metadata.dateformat}>
          <GlobalizeProvider locale={locale} currency={metadata.currency()}>
            <AuthRoute />
          </GlobalizeProvider>
        </FormattedProvider>
      </SafeAreaView>
    </>
  )
}
const BAR_HEIGHT = StatusBar.currentHeight;
const styles = StyleSheet.create({
  topSafeArea: {
    flex: 0,
    backgroundColor: '#000000'
  },
  bottomSafeArea: {
    flex: 1,
    backgroundColor: COLORS.black
  },
  statusBar: {
    height: BAR_HEIGHT
  },
});
export default App